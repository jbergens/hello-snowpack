/** @type {import("snowpack").SnowpackUserConfig } */

// Configure snowpack
module.exports = {
  mount: {
    public: {url: '/', static: false},
    src: {url: '/dist'},
  },
  plugins: ['@snowpack/plugin-dotenv', '@prefresh/snowpack'],
  routes: [
    /* Enable an SPA Fallback in development: */
    {"match": "routes", "src": ".*", "dest": "/index.html"},
  ],
  optimize: {
    /* Example: Bundle your final build: */
    // "bundle": true,
  },
  packageOptions: {
    // "source": "remote"
  },
  devOptions: {
    /* ... */
  },
  buildOptions: {
    /* ... */
  },

  alias: {
    /* ... */
  },
};
