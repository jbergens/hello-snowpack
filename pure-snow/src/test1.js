import { helloWorld } from './hello-lib.js';

function showPhrase() {
  const phrase = helloWorld();
  const pElem = document.getElementById('p-one');

  pElem.innerHTML = phrase;
}

window.onload = () => {
  showPhrase();
};

console.log('in test1.js');
