import { helloWorld } from './hello-lib.js';

function showPhrase() {
  console.log('showPhrase()');
  const phrase = helloWorld();
  const pElem = document.getElementById('p-one');

  pElem.innerHTML = phrase;
}

window.onload = () => {
  console.log('onLoad is running');
  showPhrase();
};

// showPhrase();
