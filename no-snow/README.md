No-snow Demo
============
This demo shows Preact with htm which is used without any build tool.

Simple Start
============
Server the public folder with any http server and open in a browser.
Edit files and reload.

Included http-Server
====================
To start a node based http server:

* yarn install
* yarn start
* open http://localhost:8080/ in a browser

Next time you only have to do 'yarn start'.
