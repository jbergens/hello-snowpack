# README #

This repo contains a few experiments done to try out [Snowpack](https://www.snowpack.dev/).

### Intro ###

The goal of this code is to try some different things with [Snowpack](https://www.snowpack.dev/)
with and without React. Some experiments use Babel to transpile jsx
and some use something else. One idea is to see if some minimal amount of build tools can be used
and still have a simple way to create a simple web application. The best outcome is if we can
create React apps but Preact or Vue may be ok too.

Check each subfolder for a readme for that experiment.

### Experiments ###

|Folder      |Description                                                  |Status         |
|------------|-------------------------------------------------------------|---------------|
|pure-snow   |Snowpack and html + vanillajs                                |Started        |
|full-snow   |Snowpack, React and jsx. Proably with Babel                  |               |
|no-snow     |Only Preact Htm, no bundling                                 |Started        |
